<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Basics extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->unique('email');
		});

		/**
		 * While is 1 voucher to 1 offer, I simplified the model.
		 * 
		 * To other approaches, I could make like this:
		 * 
		 * APPROACH: N vouchers to 1 offer
		 * 
		 * How it could work: create an offer named "Social Networks" and several vouchers under it: 
		 * "Facebook", "Instagram", "Pinterest", etc. Each voucher could have its own discount and 
		 * will have improved statistics about reach on social network, email, offline, etc.
		 * This approach will require another table to create the relationship between offers / vouchers
		 * 
		 */
		Schema::create('offers', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name')->default('Voucher');
			$table->integer('discount')->default(0);
			$table->char('offer_code', 8);
			$table->integer('user_id')->nullable();
			$table->date('expires_at')->nullable();
			$table->timestamp('redeemed_at')->nullable();
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->unique('id');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
