<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('TableSeeder');
	}

}

class TableSeeder extends Seeder {

	public function run() {

		DB::table('users')->truncate();
		
		$user[] = array(
			'name' => 'Lucas Moreira',
			'email' => 'moreirapontocom@gmail.com'
		);

		$user[] = array(
			'name' => 'John Doe',
			'email' => 'john@doe.com'
		);

		User::insert( $user );


		DB::table('offers')->truncate();

		$offer[] = array(
			'name' => 'social_Facebook',
			'discount' => 30,
			'offer_code' => 12345678,
			'user_id' => NULL,
			'expires_at' => '2018-10-26',
			'redeemed_at' => NULL,
		);

		$offer[] = array(
			'name' => 'social_Instagram',
			'discount' => 20.5,
			'offer_code' => 87654321,
			'user_id' => 2,
			'expires_at' => '2017-12-25',
			'redeemed_at' => date('Y-m-d'),
		);

		Offer::insert( $offer );

	}

}
