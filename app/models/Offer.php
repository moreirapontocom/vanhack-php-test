<?php

class Offer extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'offers';

	protected $guarded = array('id');

	protected function getExpiresAtAttribute($value) {
		( !empty($value) ) ?
			$value = date_format( date_create($value), 'd.m.Y' ) :
			$value = NULL;

		return $value;		
	}

	protected function getRedeemedAtAttribute($value) {
		( !empty($value) ) ?
			$value = date_format( date_create($value), 'd.m.Y' ) :
			$value = NULL;

		return $value;		
	}

}
