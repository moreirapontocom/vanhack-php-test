<?php

class AppHelper {

    public static function checkEmail($email) {
        // It's just a simulation of email checking.
        return true;
    }

    public static function generateHash($value='') {

        ( isset($value) && !empty($value) ) ?
            $key = $value :
            $key = time();

        return strtoupper( substr( md5( uniqid() . rand(1,5555) . uniqid() . rand(6666,9999) . $key ),0,8) );

    }

}