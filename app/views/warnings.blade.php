@if ( count($errors) > 0 )
    <ul>
        @foreach ( $errors->all() as $error )
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

@if ( Session::has('alerts') )
    <script>
        var warning = function() {
            Materialize.toast( "{{ Session::get('alerts') }}", 4000 );
        }();
    </script>
@endif