@extends('master')

@section('content')

    <h4>Voucher/Offers Management</h4>
    <p>
        Developed by <a href="mailto:moreirapontocom@gmail.com" target="_blank">Lucas Moreira</a> for VanHach PHP Test
    </p>

    <div class="separator separator-50"></div>

    <div class="row">
        <div class="col l4">

            @include('offers.form-create')

        </div>
        <div class="col l8">

            <h4>
                <i class="material-icons">card_giftcard</i> Offers ({{ $offers->count() }})
            </h4>
            <p>
                <strong>To redeem offers, use this link:</strong> <a href="{{ url('offers/redeem') }}" target="_blank">{{ url('offers/redeem') }}</a>
            </p>

            <div class="separator separator-30"></div>

            @if ( $offers->count() > 0 )

                <ul class="collapsible" data-collapsible="accordion">

                    @foreach ( $offers as $offer )

                        <li>
                            <div class="collapsible-header">
                                <span class="muted offer-code">#{{ $offer->offer_code }}</span>

                                @if ( empty($offer->redeemed_at) )
                                    <i class="material-icons is_redeemed">close</i> 
                                @else
                                    <i class="material-icons is_redeemed yes tooltipped"  data-position="top" data-tooltip="Redeemed at {{ $offer->redeemed_at }}">done</i> 
                                @endif

                                {{ $offer->name }}
                            </div>
                            <div class="collapsible-body">

                                <div class="row">
                                    <div class="col l6">
                                        <strong>Offer Details</strong>

                                        <div class="separator separator-10"></div>

                                        Expiration date: {{ $offer->expires_at }}<br>
                                        Discount: {{ $offer->discount }}%<br>
                                    </div>
                                    <div class="col l6">

                                        @if ( empty($offer->redeemed_at) )

                                            <strong>Available for Redeem</strong>

                                        @else

                                            <strong>Redeemed at {{ $offer->redeemed_at }}</strong>
                                            
                                            <div class="separator separator-10"></div>

                                            {{ $offer->user_name }}<br>
                                            {{ $offer->user_email }}

                                        @endif
                                    </div>
                                </div>

                                <span>

                                </span>
                            </div>
                        </li>

                    @endforeach

                </ul>

            @else

                No offers found

            @endif

        </div>
    </div>

@stop