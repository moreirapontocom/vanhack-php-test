@extends('master')

@section('content')

    <div class="align-center">

        <strong>
            Voucher redeemed!
        </strong>

        <div class="separator separator-50"></div>

        Hey <strong>{{ $user_name }}</strong>.<br>
        Congratulations! You've got <strong>{{ $voucher->discount }}%</strong> off.

    </div>

@stop