@extends('master')

@section('content')

    <div class="align-center">

        <strong>
            Voucher not redeemed.
        </strong>

        <div class="separator separator-50"></div>

        @if ( $error )

            <strong>{{ $error }}</strong>

        @endif

    </div>

@stop