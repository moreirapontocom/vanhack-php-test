@extends('master')

@section('content')

    <div class="row">
        <div class="col l4">

            <div class="card">

                <form action="{{ url('offers/check') }}" method="post">

                    <div class="card-content">

                        <strong>My vouchers</strong>

                        <p>
                            <div class="input-field">
                                <input id="userEmail" name="userEmail" type="text">
                                <label for="userEmail">My E-mail</label>
                            </div>
                        </p>

                    </div>
                    <div class="card-action teal lighten-1 center-align">
                        <button type="submit" class="btn z-depth-0"><i class="material-icons left">done</i>Check</button>
                    </div>

                </form>
            </div>

        </div>

        @if ( isset($offers) )

            <div class="col l8">

                <h4>Vouchers</h4>
                <p>
                    Your redeemed vouchers
                </p>

                <div class="separator separator-50"></div>

                <h4>
                    <i class="material-icons">card_giftcard</i> Vouchers ({{ $offers->count() }})
                </h4>

                <div class="separator separator-30"></div>

                @if ( $offers->count() > 0 )

                    <ul class="collapsible" data-collapsible="accordion">

                        @foreach ( $offers as $offer )

                            <li>
                                <div class="collapsible-header">
                                    <span class="muted offer-code">#{{ $offer->offer_code }}</span>
                                    {{ $offer->name }}
                                </div>
                                <div class="collapsible-body">

                                    <div class="row">
                                        <div class="col l6">
                                            <strong>Offer Details</strong>

                                            <div class="separator separator-10"></div>

                                            Expiration date: {{ $offer->expires_at }}<br>
                                            Discount: {{ $offer->discount }}%<br>
                                        </div>
                                        <div class="col l6">

                                            <strong>Redeemed at {{ $offer->redeemed_at }}</strong>

                                        </div>
                                    </div>

                                </div>
                            </li>

                        @endforeach

                    </ul>

                @else

                    No offers found

                @endif

            </div>

        @endif

    </div>

@stop