<div class="card">

    <form action="{{ url('offers/store') }}" method="post">

        <div class="card-content">

            <strong>New Offer</strong>

            <p>
                <div class="input-field">
                    <input id="name" name="name" type="text">
                    <label for="name">Offer Name</label>
                </div>
            </p>
            <p>
                <div class="input-field">
                    <input id="discount" name="discount" type="number">
                    <label for="discount">Discount %</label>
                </div>
            </p>
            <p>
                <div class="input-field">
                    <input type="text" name="expires_at" class="datepicker">
                    <label for="discount">Expiration date</label>
                </div>
            </p>

        </div>
        <div class="card-action teal lighten-1 center-align">
            <button type="submit" class="btn z-depth-0"><i class="material-icons left">done</i>Create Offer</button>
        </div>

    </form>
</div>