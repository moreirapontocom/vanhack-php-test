@extends('master')

@section('content')

    <div class="row">
        <div class="col l6">

            <h4>Redeem</h4>
            <p>
                Fill the fields above to redeem your discount
            </p>

            <div class="separator separator-30"></div>

            <div class="card">

                <form action="{{ url('offers/redeem') }}" method="post">

                    <div class="card-content">

                        <strong>Redeem your voucher</strong>

                        <p>
                            <div class="input-field">
                                <input id="offerHash" name="offerHash" type="text">
                                <label for="offerHash">Voucher Code</label>
                            </div>
                        </p>
                        <p>
                            <div class="input-field">
                                <input type="text" name="userName">
                                <label for="userName">Your Name</label>
                            </div>
                        </p>
                        <p>
                            <div class="input-field">
                                <input id="userEmail" name="userEmail" type="email">
                                <label for="userEmail">Your E-mail</label>
                            </div>
                        </p>

                    </div>
                    <div class="card-action teal lighten-1 center-align">
                        <button type="submit" class="btn z-depth-0"><i class="material-icons left">done</i>Redeem</button>
                    </div>

                </form>
            </div>

        </div>
        <div class="col l6">
        
            <h4>Check</h4>
            <p>
                Check your redeemed vouchers
            </p>

            <div class="separator separator-30"></div>

            <div class="card">

                <form action="{{ url('offers/check') }}" method="post">

                    <div class="card-content">

                        <strong>Check</strong>

                        <p>
                            <div class="input-field">
                                <input id="userEmail" name="userEmail" type="email">
                                <label for="userEmail">Your E-mail</label>
                            </div>
                        </p>

                    </div>
                    <div class="card-action teal lighten-1 center-align">
                        <button type="submit" class="btn z-depth-0"><i class="material-icons left">done</i>Check</button>
                    </div>

                </form>
            </div>

        </div>
    </div>

@stop