<?php

// List of available urls and its HTTP method

// GET	/offers			- Admin page to manage offers
// POST /offers/store	- Save offer
// GET	/offers/redeem	- Show view to redeem offers
// POST	/offers/redeem	- Redeem offer's logic
// GET	/offers/check	- Show view to check my vouchers
// POST	/offers/check	- Check offer's logic

Route::group(array('prefix' => 'offers'), function() {

	// Listing all offers
	Route::get('/', 'OfferController@index');

	// Storing new offer
	Route::post('store', 'OfferController@store');

	Route::get('redeem', 'OfferController@showRedeem');

	// Redeeming offer
	// Use Postman to reach this URL
	// Required params: offerHash, userEmail and userName
	Route::post('redeem', 'OfferController@redeem');

	// Check my vouchers
	Route::get('check', 'OfferController@showCheck');
	
	// Check all vouchers redeeemed by user
	Route::post('check', 'OfferController@check');

});

Route::get('/', function() {
	return Redirect::to('offers');
});