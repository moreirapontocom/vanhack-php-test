<?php

class OfferController extends \BaseController {

	/**
	 * Display a listing of offers.
	 *
	 * @return Blade view
	 */
	public function index()
	{
		$offers = '';

		// Order newest first

		// SQL Query
		/*
		SELECT
			offers.name,
			offers.discount,
			offers.offer_code,
			users.name,
			users.email,
			offers.expires_at,
			offers.redeemed_at
		FROM offers
		LEFT JOIN users ON users.id = offers.user_id
		ORDER BY offers.id DESC;
		*/

		$offers = Offer::leftJoin('users','users.id','=','offers.user_id')
						->select(
							array(
								'offers.name AS name',
								'offers.discount AS discount',
								'offers.offer_code AS offer_code',
								'users.name AS user_name',
								'users.email AS user_email',
								'offers.expires_at AS expires_at',
								'offers.redeemed_at AS redeemed_at'
							)
						)
						->orderBy('offers.id','DESC')
						->get();

		$response = array(
			'offers' => $offers
		);

		return View::make('offers.index', $response);

	}


	/**
	 * Store a newly offer.
	 *
	 * @return Response
	 */
	public function store()
	{
		$formData = Input::all();

		( !empty($formData['expires_at']) ) ?
			$formData['expires_at'] = date_format( date_create( $formData['expires_at'] ), 'Y-m-d' ) :
			$formData['expires_at'] = NULL;

		$rules = array(
			'name' => 'required|min:3|max:255',
			'discount' => 'required'
		);

		$i18n = array(
			'required'  => 'The field ":attribute" is required.',
			'min'       => 'The field ":attribute" should have at least :min length',
			'max'       => 'The field ":attribute" should have :max length as tops'
		);

		$validation = Validator::make($formData, $rules, $i18n);

		if ( $validation->fails() ) {
			return Redirect::back()
				->withErrors($validation)
				->withInput();
		}

		$formData['offer_code'] = AppHelper::generateHash();

		$offer = new Offer();
		$offer->__construct( $formData );
		$offer->save();

		return Redirect::back()->with('alerts','Offer Saved');

	}

	public function showRedeem() {
		return View::make('offers.redeem');
	}

	/**
	 * Redeem a voucher
	 * 
	 * @param offerHash String {8}
	 * @param userEmail String [email]
	 * @param userName String [text]
	 * 
	 * @return View
	 */
	public function redeem() {

		( Input::has('offerHash') ) ?
			$offerHash = Input::get('offerHash') :
			$offerHash = '';

		( Input::has('userEmail') ) ?
			$userEmail = Input::get('userEmail') :
			$userEmail = '';

		( Input::has('userName') ) ?
			$userName = urldecode( Input::get('userName') ) :
			$userName = 'Lucas Moreira';


		if ( empty($offerHash) || empty($userEmail) || empty($userName) )
			return View::make('offers.error', array('error' => 'Missing data'));

		$voucher = Offer::where('offer_code','=', $offerHash)->first();

		// Check if voucher is valid

		if ( !$voucher )
			return View::make('offers.error', array('error' => 'Voucher not found'));

		// Check if email is valid (just a simulation. Will always return true)

		if ( !AppHelper::checkEmail($userEmail) )
			return View::make('offers.error', array('error' => 'Wrong e-mail address'));

		// Check if voucher is available

		if ( !empty($voucher->redeemed_at) )
			return View::make('offers.error', array('error' => 'Voucher already redeemed'));

		// Check if voucher is expired
		// Today > expiration date

		$expiration_formated = date_format( date_create($voucher->expires_at), 'Y-m-d' );
		if ( date('Y-m-d') > $expiration_formated )
			return View::make('offers.error', array('error' => 'Voucher expired'));

		// Checking if is a known user
		// If yes: update his name and get the ID,
		// If not: create the user and get his ID
		// Assign the voucher to user

		$user = User::where('email','=',$userEmail)->first();
		if ( $user ) {
			$user_id = $user->id;

			$user->name = $userName;
			$user->save();

		} else {

			$new_user = new User();
			$new_user->name = $userName;
			$new_user->email = $userEmail;
			$new_user->save();

			$user_id = $new_user->id;

		}

		// Set voucher as redeemed

		$voucher->user_id = $user_id;
		$voucher->redeemed_at = date('Y-m-d H:i:s');
		$voucher->save();

		return View::make('offers.redeem-success', array('user_name' => $userName, 'voucher' => $voucher));

	}

	public function showCheck() {
		return View::make('offers.vouchers');
	}

	/**
	 * Check redeemed vouchers
	 * 
	 * @param userEmail String [email]
	 * 
	 * @return View
	 */
	public function check() {

		$formData = Input::all();

		$rules = array(
			'userEmail' => 'required|min:3|max:255'
		);

		$i18n = array(
			'required'  => 'The field ":attribute" is required.',
			'min'       => 'The field ":attribute" should have at least :min length',
			'max'       => 'The field ":attribute" should have :max length as tops'
		);

		$validation = Validator::make($formData, $rules, $i18n);

		if ( $validation->fails() ) {
			return Redirect::back()
				->withErrors($validation)
				->withInput();
		}

		// Check if email exist
		$user = User::where('email','=',$formData['userEmail'])->first();
		if ( !$user )
			return View::make('offers.error', array('error' => 'E-mail address not found'));

		$vouchers = Offer::join('users','users.id','=','offers.user_id')
						->where('offers.user_id','=', $user->id)
						->orderBy('offers.id','ASC')
						->get();

		return View::make('offers.vouchers', array('offers' => $vouchers));

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
