# About

The documentation is available here: [https://docs.google.com/document/d/1ojMORv9bUMjSJlRK3ghLU148iWpGdfrbIDU7VJlsXLA/edit?usp=sharing](https://docs.google.com/document/d/1ojMORv9bUMjSJlRK3ghLU148iWpGdfrbIDU7VJlsXLA/edit?usp=sharing)

### Questions

Any question, feel free to reach me at [moreirapontocom@gmail.com](moreirapontocom@gmail.com)