$(function() {
    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: 15,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false
    });
    $('.collapsible').collapsible();
    $('.modal').modal();
    $('.tooltipped').tooltip({
        delay: 50
    });
});